def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    sides = [a, b, c]
    if not any([value > 0 for value in sides]):
        return False
    max_side = max(sides)
    sides.remove(max_side)
    return sum(sides) > max_side
